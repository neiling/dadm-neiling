package es.uam.eps.dadm.othello.database;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.util.Log;

import es.uam.eps.dadm.othello.database.RoundDataBaseSchema.UserTable;
import es.uam.eps.dadm.othello.database.RoundDataBaseSchema.RoundTable;
import es.uam.eps.dadm.othello.model.Round;
import es.uam.eps.multij.ExcepcionJuego;

/**
 * Wrapper class to get the round form the database.
 */
public class RoundCursorWrapper extends CursorWrapper {

    /**
     * Empty constructor for a new instance of a RoundCursorWrapper.
     *
     * @param cursor to call the super class.
     */
    public RoundCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    /**
     * @return a round form the database.
     */
    public Round getRound() {
        String playername = getString(getColumnIndex(UserTable.Cols.PLAYERNAME));
        String playerUUID = getString(getColumnIndex(UserTable.Cols.PLAYERUUID));
        String roundID = getString(getColumnIndex(RoundTable.Cols.ROUNDUUID));
        String date = getString(getColumnIndex(RoundTable.Cols.DATE));
        String title = getString(getColumnIndex(RoundTable.Cols.TITLE));
        String size = getString(getColumnIndex(RoundTable.Cols.SIZE));
        String board = getString(getColumnIndex(RoundTable.Cols.BOARD));

        Round round = new Round(Integer.parseInt(size));
        round.setId(roundID);
        round.setFirstPlayerName("random");
        round.setSecondPlayerName(playername);
        round.setPlayerUUID(playerUUID);
        round.setDate(date);
        round.setTitle(title);

        try {
            round.getBoard().stringToTablero(board);
        } catch (ExcepcionJuego e) {
            Log.d("DEBUG", "Error turning string into tablero");
        }
        return round;
    }

}
