package es.uam.eps.dadm.othello.model;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Movimiento;
import es.uam.eps.multij.Tablero;

import java.util.ArrayList;

/**
 * @author neiling
 */
public class TableroOthello extends Tablero {

    /**
     * Default board size.
     */
    public static final int DEFAULT_BOARD_SIZE = 8;

    /**
     * El tamaño de tablero, por defecto es 8x8.
     */
    private int tableroSize;

    /**
     * Almacena la ubicación de cada ficha.
     */
    private Fichas tablero[][];

    /**
     * La cantidad de fichas blancas.
     */
    private int countFichasBlancas;

    /**
     * La cantidad de fichas negras.
     */
    private int countFichasNegras;

    /**
     * Constructor para una nueva instancia de TableroOthello.
     *
     * @param tableroSize new size of the borad.
     */
    public TableroOthello(int tableroSize) {
        this.tableroSize = tableroSize;
        this.tablero = cargaTablero(new Fichas[tableroSize][tableroSize]);
        this.estado = 1;
        countFichas();
    }

    /**
     * Constructor para una nueva instancia de TableroOthello
     * con El tamaño de tablero, por defecto.
     */
    public TableroOthello() {
        this.tableroSize = DEFAULT_BOARD_SIZE;
        this.tablero = cargaTablero(new Fichas[tableroSize][tableroSize]);
        this.estado = 1;
        countFichas();
    }

    /**
     * Llena el tablero con fichas vacío.
     *
     * @param tablero un de fichas.
     * @return un tablero con posicion de inicio por defecto.
     */
    private Fichas[][] cargaTablero(Fichas[][] tablero) {
        for (int filaNu = 0; filaNu < tablero.length; filaNu++) {
            for (int colNu = 0; colNu < tablero[0].length; colNu++) {
                tablero[filaNu][colNu] = Fichas.VACIO;
            }
        }

        tablero[tablero.length / 2 - 1][tablero[0].length / 2 - 1] = Fichas.BLANCO;
        tablero[tablero.length / 2][tablero[0].length / 2] = Fichas.BLANCO;
        tablero[tablero.length / 2 - 1][tablero[0].length / 2] = Fichas.NEGRO;
        tablero[tablero.length / 2][tablero[0].length / 2 - 1] = Fichas.NEGRO;

        return tablero;
    }

    /**
     * Coloca una ficha en el tablero.
     *
     * @param m un movimiento con las coordenadas.
     */
    private void placeFicha(Movimiento m) {
        int col = m.toString().charAt(0) - 'A';
        int fila = m.toString().charAt(1) - '0';

        if (Fichas.fromInteger(this.turno) == Fichas.NEGRO) {
            this.tablero[fila][col] = Fichas.NEGRO;
        } else if (Fichas.fromInteger(this.turno) == Fichas.BLANCO) {
            this.tablero[fila][col] = Fichas.BLANCO;
        }
    }

    /**
     * Execute a move on the tablero.
     *
     * @param m move to execute
     */
    @Override
    protected void mueve(Movimiento m) throws ExcepcionJuego {
        if (m != null) {
            // Order is here important, first to change then to place.
            cambioFichas(m, true);
            placeFicha(m);
            this.ultimoMovimiento = m;
        } else {
            System.out.println("Player pass this turn.");
            this.ultimoMovimiento = new MovimientoOthello("¡Paso!");
        }
        this.cambiaTurno();
        if (movimientosValidos().isEmpty()) {
            this.estado = 0;
        }
        countFichas();
    }

    @Override
    public boolean esValido(Movimiento m) {
        return cambioFichas(m, false);
    }

    /**
     * Cambiar fichas del enemigo por un movimiento.
     * <p>
     * Disclaimer: Strongly inspired from
     * github.com/winmhtun/Gomoku---Othello-JAVA/blob/master/src/GameBoard.java
     * It fits too well and I don't see really something to improve.
     *
     * @param m        Coordenadas con el punto de inicio.
     * @param ejecutar Cuando es false, entonces sólo un cheque si es posible.
     * @return true si es posible cambiar las fichas.
     */
    private boolean cambioFichas(Movimiento m, boolean ejecutar) {
        int col = m.toString().charAt(0) - 'A';
        int fila = m.toString().charAt(1) - '0';
        boolean esValida = false;

        if (this.tablero[fila][col] == Fichas.VACIO) {
            for (int deltaFila = -1; deltaFila < 2; deltaFila++) {
                for (int deltaCol = -1; deltaCol < 2; deltaCol++) {
                    if (deltaFila == 0 && deltaCol == 0) {
                        continue;
                    }

                    int tmpFila = fila + deltaFila;
                    int tmpCol = col + deltaCol;

                    if (tmpFila >= 0 && tmpFila <= tableroSize - 1 &&
                            tmpCol >= 0 && tmpCol <= tableroSize - 1) {
                        if ((this.tablero[tmpFila][tmpCol]) ==
                                (Fichas.fromInteger(this.turno) ==
                                        Fichas.NEGRO ? Fichas.BLANCO : Fichas.NEGRO)) {
                            for (int alcance = 0; alcance < tableroSize; alcance++) {

                                int nFila = fila + alcance * deltaFila;
                                int nCol = col + alcance * deltaCol;
                                if (nFila < 0 || nFila > tableroSize - 1 ||
                                        nCol < 0 || nCol > tableroSize - 1) {
                                    continue;
                                }

                                if (this.tablero[nFila][nCol].ordinal() == this.turno) {
                                    if (ejecutar) {
                                        for (int flipDistancia = 1;
                                             flipDistancia < alcance; flipDistancia++) {
                                            int finalFila = fila + flipDistancia * deltaFila;
                                            int finalCol = col + flipDistancia * deltaCol;
                                            this.tablero[finalFila][finalCol] =
                                                    Fichas.fromInteger(this.turno);
                                        }
                                    }
                                    esValida = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        return esValida;
    }

    /**
     * Count the seeds form each player.
     */
    private void countFichas() {
        this.countFichasBlancas = 0;
        this.countFichasNegras = 0;

        for (int filaNu = 0; filaNu < this.tablero.length; filaNu++) {
            for (int colNu = 0; colNu < this.tablero[0].length; colNu++) {
                if (this.tablero[filaNu][colNu] == Fichas.BLANCO) {
                    this.countFichasBlancas++;
                }
                if (this.tablero[filaNu][colNu] == Fichas.NEGRO) {
                    this.countFichasNegras++;
                }
            }
        }
    }

    @Override
    public ArrayList<Movimiento> movimientosValidos() {
        ArrayList<Movimiento> movimientosValidos = new ArrayList<>();
        MovimientoOthello m;

        for (int filaNu = 0; filaNu < this.tablero.length; filaNu++) {
            for (int colNu = 0; colNu < this.tablero[0].length; colNu++) {
                m = new MovimientoOthello("" + (char) (colNu + 'A') + filaNu);
                if (esValido(m)) {
                    movimientosValidos.add(m);
                }
            }
        }
        return movimientosValidos;
    }

    /**
     * @param yPos of the board.
     * @param xPos of the borad.
     * @return true if play able.
     */
    public boolean isPlayAble(int yPos, int xPos) {
        MovimientoOthello m;
        m = new MovimientoOthello("" + (char) (xPos + 'A') + yPos);
        return movimientosValidos().contains(m);
    }

    /* At first I wrote an own parser but then I decide it's more comfortable to
    *  use the Gson library. I hope that's ok.
    *  I forgot to ask if it's allowed to use other libraries.
    */
    @Override
    public String tableroToString() {
        return new Gson().toJson(this);
    }

    @Override
    public void stringToTablero(String cadena) throws ExcepcionJuego {
        /*
        * Gson has problems to parse the ultimoMovimiento.
        * Here a quit and dirty solution.
        */
        String[] split = cadena.split(",\"ultimoMovimiento\":");
        if (split.length > 1) {
            cadena = split[0] + "}\n";
        }
        TableroOthello t = new Gson().fromJson(cadena, TableroOthello.class);
        this.tablero = t.getTablero();
        this.turno = t.getTurno();
        this.estado = t.getEstado();
        this.numJugadas = t.getNumJugadas();
        countFichas();
    }

    @Override
    public String toString() {
        String newline = "\n";
        String withspace = " ";
        String tableroAsString = "  ";

        for (int filaNu = 'A'; filaNu < this.tablero.length + 'A'; filaNu++) {
            tableroAsString = tableroAsString + (char) filaNu + withspace;
        }
        tableroAsString = tableroAsString + newline;
        for (int filaNu = 0; filaNu < this.tablero.length; filaNu++) {
            tableroAsString = tableroAsString + filaNu + withspace;
            for (int colNu = 0; colNu < this.tablero[0].length; colNu++) {
                switch (this.tablero[filaNu][colNu]) {
                    case NEGRO:
                        tableroAsString = tableroAsString + "N ";
                        break;
                    case BLANCO:
                        tableroAsString = tableroAsString + "B ";
                        break;
                    case VACIO:
                        tableroAsString = tableroAsString + withspace;
                        break;
                    default:
                }
            }
            tableroAsString = tableroAsString + newline;
        }
        return tableroAsString;
    }

    /**
     * @return A string with information about the current game state.
     * */
    public String toSimpleString() {
        countFichas();
        return "Black " + this.countFichasNegras + " White " + this.countFichasBlancas;
    }

    /**
     * @return a string with current information about the game.
     */
    public String gameState() {
        return "";
    }

    /**
     * @param yPos of the position.
     * @param xPos of the position.
     * @return The seeds form a certain position.
     */
    public Fichas getTableroAt(int yPos, int xPos) {
        return this.tablero[yPos][xPos];
    }

    public Fichas[][] getTablero() {
        return tablero;
    }

    public int getTableroSize() {
        return tableroSize;
    }

    public void setTableroSize(int tableroSize) {
        this.tableroSize = tableroSize;
    }

    public int getCountFichasBlancas() {
        return countFichasBlancas;
    }

    public int getCountFichasNegras() {
        return countFichasNegras;
    }
}
