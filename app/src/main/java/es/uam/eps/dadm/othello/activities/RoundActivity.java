package es.uam.eps.dadm.othello.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import es.uam.eps.dadm.othello.R;
import es.uam.eps.dadm.othello.model.Round;

/**
 * Activity witch start a new game.
 */
public class RoundActivity extends AppCompatActivity implements RoundFragment.Callbacks {
    /**
     * Debug string.
     */
    private static final String DEBUG = "RoundActivity";


    /**
     * Empty constructor for a new instance of a RoundActivity.
     */
    public RoundActivity() {
    }

    /**
     * Create a new Intent to start the RoundActivity form outside.
     *
     * @param roundId         for this fragment.
     * @param firstPlayerName name of the first player.
     * @param roundTitle      title of the round.
     * @param roundSize       size of the board.
     * @param roundDate       date when the round started.
     * @param roundBoard      board of the round.
     * @param packageContext  point form where the activity should be start.
     * @return an Intent with the RoundActivity.
     */
    public static Intent newIntent(Context packageContext, String roundId, String firstPlayerName,
                                   String roundTitle, int roundSize,
                                   String roundDate, String roundBoard) {
        Intent intent = new Intent(packageContext, RoundActivity.class);
        intent.putExtra(RoundFragment.ARG_ROUND_ID, roundId);
        intent.putExtra(RoundFragment.ARG_FIRST_PLAYER_NAME, firstPlayerName);
        intent.putExtra(RoundFragment.ARG_ROUND_TITLE, roundTitle);
        intent.putExtra(RoundFragment.ARG_ROUND_SIZE, "" + roundSize);
        intent.putExtra(RoundFragment.ARG_ROUND_DATE, roundDate);
        intent.putExtra(RoundFragment.ARG_ROUND_BOARD, roundBoard);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        FragmentManager fm = getFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);


        if (fragment == null) {
            String roundId = getIntent().getStringExtra(RoundFragment.ARG_ROUND_ID);
            String firstPlayerName =
                    getIntent().getStringExtra(RoundFragment.ARG_FIRST_PLAYER_NAME);
            String roundTitle = getIntent().getStringExtra(RoundFragment.ARG_ROUND_TITLE);
            String roundSize = getIntent().getStringExtra(RoundFragment.ARG_ROUND_SIZE);
            String roundDate = getIntent().getStringExtra(RoundFragment.ARG_ROUND_DATE);
            String roundBoard = getIntent().getStringExtra(RoundFragment.ARG_ROUND_BOARD);


            RoundFragment roundFragment = RoundFragment.newInstance(roundId, firstPlayerName,
                    roundTitle, roundSize, roundDate, roundBoard);


            getFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, roundFragment)
                    .commit();
        }
    }

    @Override
    public void onRoundUpdated(Round round) {
        Log.d(DEBUG, "round updated");
    }
}
