package es.uam.eps.dadm.othello.model;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.support.annotation.Nullable;

import es.uam.eps.dadm.othello.activities.OtPreferenceActivity;
import es.uam.eps.dadm.othello.database.OthelloDataBase;
import es.uam.eps.dadm.othello.server.ServerRepository;

/**
 * A factory to create a round repository.
 */
public class RoundRepositoryFactory {

    /**
     * Empty constructor for a new instance of RoundRepositoryFactory.
     */
    private RoundRepositoryFactory() {
    }

    /**
     * @param context of calling.
     * @return a new RoundRepository.
     */
    @Nullable
    public static RoundRepository createRepository(Context context) {
        RoundRepository repository;

         boolean local = !OtPreferenceActivity.getOnline();

        repository = local ? new OthelloDataBase(context) : ServerRepository.getInstance(context);

        try {
              repository.open();
        } catch (SQLiteException e) {
            return null;
        }
        return repository;
    }

}
