package es.uam.eps.dadm.othello.server;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import es.uam.eps.dadm.othello.activities.OtPreferenceActivity;
import es.uam.eps.dadm.othello.model.Round;
import es.uam.eps.dadm.othello.model.RoundRepository;
import es.uam.eps.dadm.othello.model.TableroOthello;
import es.uam.eps.multij.ExcepcionJuego;

public class ServerRepository implements RoundRepository {
    private static final String DEBUG = "ServerRepository";
    private static ServerRepository repository;
    private final Context context;
    private ServerInterface is;

    public static ServerRepository getInstance(Context context) {
        if (repository == null)
            repository = new ServerRepository(context.getApplicationContext());
        return repository;
    }

    private ServerRepository(Context context) {
        this.context = context.getApplicationContext();
        is = ServerInterface.getServer(context);
    }

    @Override
    public void open() {
    }

    @Override
    public void close() {
    }

    public void loginOrRegister(final String playerName, String password,
                                boolean register,
                                final RoundRepository.LoginRegisterCallback callback) {
        is.login(playerName, password, null, register,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        String uuid = result.trim();
                        if (uuid.equals("-1") || uuid.length() < 10) {
                            callback.onError("Error loggin in user " + playerName);
                        } else {
                            callback.onLogin(uuid);
                            Log.d(DEBUG, "Logged in: " + result.trim());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error.getLocalizedMessage());
                    }
                });
    }

    @Override
    public void login(String playerName, String password,
                      final RoundRepository.LoginRegisterCallback callback) {
        loginOrRegister(playerName, password, false, callback);
    }

    @Override
    public void register(String playerName, String password,
                         RoundRepository.LoginRegisterCallback callback) {
        loginOrRegister(playerName, password, true, callback);
    }

    private List<Round> roundsFromJSONArray(JSONArray response) {
        List<Round> rounds = new ArrayList<>();
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject o = response.getJSONObject(i);
                String roundID = o.getString("roundid");
                int numberOfPlayers = o.getInt("numberofplayers");
                String date = o.getString("dateevent");
                String[] playernames = o.getString("playernames").split(",");
                int turn = o.getInt("turn");
                String codedboard = o.getString("codedboard");

                if (codedboard.length() > 0) {
                    TableroOthello tableroOthello = new TableroOthello();
                    tableroOthello.stringToTablero(codedboard);
                    Round round = new Round(tableroOthello.getTableroSize(),
                            OtPreferenceActivity.getPlayerUUID(this.context));
                    round.setBoard(tableroOthello);
                    if (numberOfPlayers > 1) {
                        round.setFirstPlayerName(playernames[0]);
                        round.setSecondPlayerName(playernames[1]);
                    } else {
                        round.setFirstPlayerName(playernames[0]);
                        round.setSecondPlayerName("vacant");
                    }
                    round.setId(roundID);
                    round.setDate(date);
                    round.setTitle(roundID);
                    round.setNumbersOfPlayer(numberOfPlayers);
                    rounds.add(round);

                }
            } catch (JSONException | ExcepcionJuego e) {
                e.printStackTrace();
            }
        }
        return rounds;
    }


    @Override
    public void getRounds(String playeruuid, String orderByField, String group,
                          final RoundsCallback callback) {
        Response.Listener<JSONArray> responseCallback = new
                Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        List<Round> rounds = roundsFromJSONArray(response);
                        callback.onResponse(rounds);
                        Log.d(DEBUG, "Rounds downloaded from server");
                    }
                };
        Response.ErrorListener errorCallback = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError("Error downloading rounds from server");
            }
        };
        //amdis.getOpenRounds(playeruuid, responseCallback, errorCallback);
        is.getActiveRounds(playeruuid, responseCallback, errorCallback);
    }

    public void getActiveRounds(String puuid, final RoundsCallback callback) {

    }

    public void getAllRounds(final String playeruuid, final RoundsCallback callback) {

    }

    @Override
    public void addRound(final Round round, final BooleanCallback callback) {
        Response.Listener<String> responseCallback = new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {
                if (callback != null) {
                    callback.onResponse(Integer.valueOf(result) > 0);
                }
            }
        };
        Response.ErrorListener errorCallback = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getLocalizedMessage());
            }
        };
        is.newRound(round.getPlayerUUID(),round.getBoard().tableroToString(),
                responseCallback, errorCallback);
    }

    @Override
    public void updateRound(Round round, BooleanCallback callback) {

    }

    public void addPlayerToRound(final Round round, final BooleanCallback callback) {
        Response.Listener<String> responseCallback = new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {
                if (callback != null) {
                    callback.onResponse(Integer.valueOf(result) > 0);
                }
            }
        };
        Response.ErrorListener errorCallback = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getLocalizedMessage());
            }
        };
        is.addPlayerToRound(Integer.valueOf(round.getId()),
                round.getPlayerUUID(),responseCallback,errorCallback);
    }
}
