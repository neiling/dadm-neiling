package es.uam.eps.dadm.othello.activities;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import es.uam.eps.dadm.othello.R;

/**
 * Created by am on 5/12/17.
 */
public class HelpFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        final View helpView = inflater.inflate(R.layout.help_fragment, container, false);
        TextView helpTextView = (TextView)
                helpView.findViewById(R.id.help_text);
        helpTextView.setText(Html.fromHtml(getString(R.string.help_text)));
        return helpView;
    }

}
