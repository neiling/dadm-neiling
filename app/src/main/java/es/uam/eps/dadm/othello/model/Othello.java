package es.uam.eps.dadm.othello.model;

import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.JugadorAleatorio;
import es.uam.eps.multij.Partida;
import es.uam.eps.multij.Tablero;

import java.util.ArrayList;


/**
 * Connect four the classic game.
 *
 * @author neiling
 */
public class Othello {

    /**
     * Empty constructor for a new instance of Othello.
     */
    private Othello() {
    }

    /**
     * Entry point for starting the game.
     *
     * @param args an array of arguments
     */
    public static void main(String[] args) {
        Jugador jugadorHumano = new JugadorHumano("Humano");
        Jugador jugadorAleatorio = new JugadorAleatorio("Máquina");
        ArrayList<Jugador> jugadores = new ArrayList<>();
        jugadores.add(jugadorHumano);
        jugadores.add(jugadorAleatorio);

        Partida partida = new Partida(new TableroOthello(), jugadores);
        partida.addObservador(new JugadorHumano());
        Tablero tablero = partida.getTablero();
        partida.comenzar();
    }


}
