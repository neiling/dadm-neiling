package es.uam.eps.dadm.othello.model;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import es.uam.eps.dadm.othello.activities.OtPreferenceActivity;
import es.uam.eps.dadm.othello.server.ServerInterface;
import es.uam.eps.dadm.othello.views.OthelloView;
import es.uam.eps.multij.AccionMover;
import es.uam.eps.multij.Evento;
import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.Partida;
import es.uam.eps.multij.Tablero;

public class OtLocalServerPlayer implements Jugador, OthelloView.OnPlayListener {

    private static final String DEBUG = "OtLocalServerPlayer";
    private Partida game;
    private Context context;
    private String roundId;
    private Jugador jugador = this;

    public OtLocalServerPlayer(Context context, String roundId) {
        this.context = context;
        this.roundId = roundId;
    }


    @Override
    public void onPlay(final int yPos, final int xPos) {
        ServerInterface is = ServerInterface.getServer(context);
        final String playerId = OtPreferenceActivity.getPlayerUUID(this.context);
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                /*
                * The server use apparently to different parsing methods.
                * There are problems to deserialize the codeboard because the \" are missing.
                * Therefore, here a quick and dirty solution.
                */
                String[] split = response.split(", \"codedboard\" : \"");
                int turn;
                try {
                    turn = new JSONObject(split[0] + "}\n").getInt("turn");
                    game.getTablero().stringToTablero(split[1].substring(0, split[1].length() - 3));
                    if (game.getTablero().getEstado() != Tablero.EN_CURSO) {
                        return;
                    }
                    MovimientoOthello m;
                    m = new MovimientoOthello("" + (char) (xPos + 'A') + yPos);
                    if (game.getTablero().esValido(m) && turn == 1) {
                        game.realizaAccion(new AccionMover(jugador, m));
                    } else {
                        Log.d(DEBUG, "Invalid input or not your turn.");
                    }
                } catch (JSONException | ExcepcionJuego e) {
                    e.printStackTrace();
                }
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(DEBUG, "Error: " + error);
            }
        };
        is.isMyTurn(Integer.parseInt(roundId), playerId, responseListener,
                errorListener);
    }

    @Override
    public String getNombre() {
        return null;
    }

    @Override
    public boolean puedeJugar(Tablero tablero) {
        return true;
    }

    @Override
    public void onCambioEnPartida(Evento evento) {

    }

    public Partida getGame() {
        return game;
    }

    public void setGame(Partida game) {
        this.game = game;
    }

}