package es.uam.eps.dadm.othello.model;

/**
 * Representa los estados de una ficha.
 *
 * @author neiling
 */
public enum Fichas {

    /**
     * Ficha negro.
     */
    NEGRO,
    /**
     * Ficha blanco.
     */
    BLANCO,
    /**
     * Ficha vacio.
     */
    VACIO;

    /**
     * Genera una ficha de un integer.
     * @param x negro = 0, blanco = 1, vacio = 2
     * @return una ficha negro, blanco o vacio.
     */
    public static Fichas fromInteger(int x) {
        switch (x) {
            case 0:
                return NEGRO;
            case 1:
                return BLANCO;
            case 2:
                return VACIO;
            default:
                System.out.println("Error: Undefined ficha type");
                return null;
        }
    }
}
