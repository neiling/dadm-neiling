package es.uam.eps.dadm.othello.activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import es.uam.eps.dadm.othello.R;

/**
 * Contains the Preferences of a player.
 */
public class OtPreferenceActivity extends AppCompatActivity {

    /**
     * Key to receive the board size.
     */
    public static final String BOARDSIZE_KEY = "boardsize";

    /**
     * Default size of the board.
     */
    public static final String BOARDSIZE_DEFAULT = "8";

    /**
     * Default player name.
     */
    public static final String PLAYERNAME_DEFAULT = "default";

    /**
     * The UUID of the player.
     */
    private static String playerUUID;

    /**
     * Name of the player.
     */
    private static String playerName;

    /**
     * Name of the player.
     */
    private static String playerPassword;

    /**
     * If the player is online.
     */
    private static Boolean online;

    /**
     * Empty constructor for a new instance of a OtPreferenceFragment.
     */
    public OtPreferenceActivity() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        OtPreferenceFragment fragment = new OtPreferenceFragment();
        fragmentTransaction.replace(android.R.id.content, fragment);
        fragmentTransaction.commit();
    }

    /**
     * @param context of the call.
     * @return the board size form the app preference.
     */
    public static String getBoardSize(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(BOARDSIZE_KEY, BOARDSIZE_DEFAULT);
    }

    /**
     * @param context of the call.
     * @param size    of the board.
     */
    public static void setBoardsize(Context context, int size) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(OtPreferenceActivity.BOARDSIZE_KEY, size);
        editor.commit();
    }

    /**
     * @param loginActivity I have no idea what I should do with that.
     * @param playerId      of the player.
     */
    public static void setPlayerUUID(LoginActivity loginActivity, String playerId) {
        playerUUID = playerId;
    }

    /**
     * @param loginActivity I have no idea what I should do with that.
     * @param playername    of the player.
     */
    public static void setPlayerName(LoginActivity loginActivity, String playername) {
        playerName = playername;
    }

    /**
     * @param loginActivity I have no idea what I should do with that.
     * @param password      of the player.
     */
    public static void setPlayerPassword(LoginActivity loginActivity, String password) {
        playerPassword = password;
    }

    /**
     * @param activity I have no idea what I should do with that.
     * @return id of the player.
     */
    public static String getPlayerUUID(Activity activity) {
        return playerUUID;
    }

    /**
     * @param context I have no idea what I should do with that.
     * @return id of the player.
     */
    public static String getPlayerUUID(Context context) {
        return playerUUID;
    }

    public static void setOnline(Activity activity , Boolean playerOnline) {
        OtPreferenceActivity.online = playerOnline;
    }

    public static Boolean getOnline() {
        return online;
    }

    public static String getPlayerName() {
        return playerName;
    }
}
