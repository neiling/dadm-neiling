package es.uam.eps.dadm.othello.activities;

import es.uam.eps.dadm.othello.model.MovimientoOthello;
import es.uam.eps.dadm.othello.views.OthelloView;
import es.uam.eps.multij.AccionMover;
import es.uam.eps.multij.Evento;
import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.Partida;
import es.uam.eps.multij.Tablero;

/**
 * Class for the local player from the app.
 *
 * @author neiling
 */

public class OthelloLocalPlayer implements OthelloView.OnPlayListener, Jugador {

    /**
     * Contains the current game.
     */
    private Partida game;

    /**
     * Empty constructor for a new instance of OthelloLocalPlayer.
     */
    public OthelloLocalPlayer() {
    }

    @Override
    public void onPlay(int yPos, int xPos) {
        try {
            if (this.game.getTablero().getEstado() != Tablero.EN_CURSO) {
                return;
            }
            MovimientoOthello m;
            m = new MovimientoOthello("" + (char) (xPos + 'A') + yPos);
            if (this.game.getTablero().esValido(m)) {
                game.realizaAccion(new AccionMover(this, m));
            } else {
                System.out.println("Invalid input!");
            }
        } catch (ExcepcionJuego e) {
            System.out.println("Error in onPlay: " + e.getMessage());
        }
    }

    @Override
    public String getNombre() {
        return "Local Player";
    }

    @Override
    public boolean puedeJugar(Tablero tablero) {
        return true;
    }

    @Override
    public void onCambioEnPartida(Evento evento) {
    }

    public Partida getGame() {
        return game;
    }

    public void setGame(Partida game) {
        this.game = game;
    }


}
