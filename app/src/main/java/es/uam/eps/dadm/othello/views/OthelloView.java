package es.uam.eps.dadm.othello.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.design.widget.Snackbar;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import es.uam.eps.dadm.othello.R;
import es.uam.eps.dadm.othello.model.Fichas;
import es.uam.eps.dadm.othello.model.TableroOthello;
import es.uam.eps.multij.Tablero;

/**
 * @author neiling
 */
public class OthelloView extends View {

    /**
     * Radius of the circle in dependence on the width or height of the tile.
     */
    private static final float DELTA_RADIUS_CIRCLE = 0.4f;

    /**
     * Background paint for different objects.
     */
    private Paint backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * Line paint for different objects.
     */
    private Paint linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * Height of a single tile of the board.
     */
    private float heightOfTile;

    /**
     * Width of a single tile of the board.
     */
    private float widthOfTile;

    /**
     * Radio of the circles.
     */
    private float radio;

    /**
     * Size of the game board.
     */
    private int size;

    /**
     * The game board.
     */
    private TableroOthello board;

    /**
     * On play listener.
     */
    private OnPlayListener onPlayListener;

    /**
     * A listener that will calling for a move.
     */
    public interface OnPlayListener {
        /**
         * Execute a move.
         * @param row of the input.
         * @param column of the input.
         */
        void onPlay(int row, int column);
    }

    /**
     * Constructor for a new instance of an OthelloView.
     * @param context of calling.
     * @param attrs attributes of the view.
     */
    public OthelloView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    /**
     * Initialization of the othello view.
     */
    private void init() {
        this.backgroundPaint.setColor(Color.DKGRAY);
        this.linePaint.setStrokeWidth(2);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //int desiredWidth = 500;
        //String wMode, hMode;
        //int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        //int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;
        if (widthSize < heightSize) {
            width = height = heightSize = widthSize;
        } else {
            width = height = widthSize = heightSize;
        }
        setMeasuredDimension(width, height);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.board.getEstado() != Tablero.EN_CURSO) {
            Snackbar.make(findViewById(R.id.board_othelloview), R.string.round_already_finished,
                    Snackbar.LENGTH_SHORT).show();
            return super.onTouchEvent(event);
        }
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            onPlayListener.onPlay(fromEventToYPos(event), fromEventToXPos(event));
        }
        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float boardWitdh = getWidth();
        float boardHeight = getHeight();
        canvas.drawRect(0, 0, boardWitdh, boardHeight, this.backgroundPaint);
        drawCircles(canvas, this.linePaint);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {


        if (this.size == 0) {
            System.out.println("ERROR: size is zero!");
        } else {
            this.widthOfTile = w / this.size;
            this.heightOfTile = h / this.size;
        }
        if (this.widthOfTile < this.heightOfTile) {
            this.radio = this.widthOfTile * DELTA_RADIUS_CIRCLE;
        } else {
            this.radio = this.heightOfTile * DELTA_RADIUS_CIRCLE;
        }
        super.onSizeChanged(w, h, oldw, oldh);
    }

    /**
     * @param canvas to draw.
     * @param paint of the canvas.
     */
    private void drawCircles(Canvas canvas, Paint paint) {
        float centerRow, centerColumn;
        for (int yPos = 0; yPos < this.size; yPos++) {
            centerRow = this.heightOfTile * (1 + 2 * yPos) / 2f;
            for (int xPos = 0; xPos < this.size; xPos++) {
                centerColumn = this.widthOfTile * (1 + 2 * xPos) / 2f;
                setPaintColor(paint, yPos, xPos);
                canvas.drawCircle(centerColumn, centerRow, this.radio, paint);
            }
        }
    }

    /**
     * @param paint of the seed.
     * @param xPos  of the seed.
     * @param yPos of the seed.
     */
    private void setPaintColor(Paint paint, int yPos, int xPos) {
        if (this.board.getTableroAt(yPos, xPos) == Fichas.BLANCO) {
            paint.setColor(Color.WHITE);
        } else if (this.board.getTableroAt(yPos, xPos) == Fichas.NEGRO) {
            paint.setColor(Color.BLACK);
        } else if (this.board.isPlayAble(yPos, xPos)) {
            paint.setColor(Color.RED);
        } else {
            paint.setColor(Color.GRAY);
        }
    }

    /**
     * @param event to change form an touch input to y position.
     * @return the y position as int.
     */
    private int fromEventToYPos(MotionEvent event) {
        return (int) (event.getY() / this.heightOfTile);
    }

    /**
     * @param event to change form an touch input to y position.
     * @return the x position as int.
     */
    private int fromEventToXPos(MotionEvent event) {
        return (int) (event.getX() / this.heightOfTile);
    }

    public OnPlayListener getOnPlayListener() {
        return onPlayListener;
    }

    public void setOnPlayListener(OnPlayListener onPlayListener) {
        this.onPlayListener = onPlayListener;
    }

    public TableroOthello getBoard() {
        return board;
    }

    /**
     * @param board of an othello match.
     */
    public void setBoard(TableroOthello board) {
        this.size = board.getTableroSize();
        this.board = board;
    }

}