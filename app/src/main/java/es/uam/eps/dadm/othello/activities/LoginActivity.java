package es.uam.eps.dadm.othello.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import es.uam.eps.dadm.othello.R;
import es.uam.eps.dadm.othello.model.RoundRepository;
import es.uam.eps.dadm.othello.model.RoundRepositoryFactory;

/**
 * Class of the login mask.
 */
public class LoginActivity extends Activity implements View.OnClickListener {

    /**
     * Repository of the rounds.
     */
    private RoundRepository repository;

    /**
     * Text field for the username.
     */
    private EditText usernameEditText;

    /**
     * Text field for the password.
     */
    private EditText passwordEditText;

    /**
     * Empty constructor for a new instance of a RoundCursorWrapper.
     */
    public LoginActivity() {
    }

    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        OtPreferenceActivity.setOnline(this, false);
        if (isOnline()) {
            OtPreferenceActivity.setOnline(this, true);
        }

        this.usernameEditText = (EditText) findViewById(R.id.login_username);
        this.passwordEditText = (EditText) findViewById(R.id.login_password);
        Button loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(this);

        Button newUserButton = (Button) findViewById(R.id.new_user_button);
        newUserButton.setOnClickListener(this);

        this.repository = RoundRepositoryFactory.createRepository(LoginActivity.this);
        if (this.repository == null) {
            Toast.makeText(LoginActivity.this, R.string.repository_opening_error,
                    Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onClick(View v) {
        final String playername = this.usernameEditText.getText().toString();
        final String password = this.passwordEditText.getText().toString();
        String regex = "[a-zA-Z0-9\\._\\-]{3,}";
        RoundRepository.LoginRegisterCallback loginRegisterCallback =
                new RoundRepository.LoginRegisterCallback() {
                    @Override
                    public void onLogin(String playerId) {
                        OtPreferenceActivity.setPlayerUUID(LoginActivity.this, playerId);
                        OtPreferenceActivity.setPlayerName(LoginActivity.this, playername);
                        OtPreferenceActivity.setPlayerPassword(LoginActivity.this, password);
                        startActivity(new Intent(LoginActivity.this, RoundListActivity.class));
                        finish();
                    }

                    @Override
                    public void onError(String error) {
                        Toast toast = Toast.makeText(LoginActivity.this, error, Toast.LENGTH_LONG);
                        toast.show();
                    }
                };
        switch (v.getId()) {
            case R.id.login_button:
                this.repository.login(playername, password, loginRegisterCallback);
                break;
            case R.id.new_user_button:
                if (playername.matches(regex) && password.matches(regex)) {
                    this.repository.register(playername, password, loginRegisterCallback);
                } else {
                    Toast.makeText(LoginActivity.this, R.string.error_registration,
                            Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                Log.e("ERROR", "Some thing went wrong at login activity.");
        }
    }

}
