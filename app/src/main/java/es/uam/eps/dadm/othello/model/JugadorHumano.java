package es.uam.eps.dadm.othello.model;

import es.uam.eps.multij.AccionMover;
import es.uam.eps.multij.Evento;
import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.Movimiento;
import es.uam.eps.multij.Tablero;

import java.util.Scanner;

/**
 * @author neiling
 */
public class JugadorHumano implements Jugador {

    /**
     * Count of the tries for wrong input.
     */
    private static final int MAX_TRIES = 3;

    /**
     * Number of Human players.
     */
    private static int numHumanos = 0;

    /**
     * Name of the player.
     */
    private String nombre;


    /**
     * Default constructor for a new instance of MovimientoOthello.
     */
    public JugadorHumano() {
        this("Humano " + (++numHumanos));
    }

    /**
     * Constructor for a new instance of MovimientoOthello.
     *
     * @param nombre of the player
     */
    public JugadorHumano(String nombre) {
        this.nombre = nombre;
    }


    @Override
    public String getNombre() {
        return this.nombre;
    }

    @Override
    public boolean puedeJugar(Tablero tablero) {
        return true;
    }

    @Override
    public void onCambioEnPartida(Evento evento) {
        /*
         * Dirty solution so that only the observer prints.
         * Is there another way to check if an observer or player is execute the code?
         */
        boolean observador = this.nombre.endsWith("1");
        Tablero tablero = evento.getPartida().getTablero();

        switch (evento.getTipo()) {
            case Evento.EVENTO_CAMBIO:
                if (observador) {

                    System.out.println(evento.getDescripcion());
                    System.out.println(tablero.toString());
                }
                break;
            case Evento.EVENTO_TURNO:
                Scanner in = new Scanner(System.in);
                String movimento;
                int count = 0;


                System.out.println(evento.getDescripcion() + ", movimientos posibles:");
                //Print possible movements.
                for (Movimiento m : evento.getPartida().getTablero().movimientosValidos()) {
                    System.out.print(m.toString() + "; ");
                }
                System.out.println();

                while (true) {
                    try {
                        movimento = in.next();
                        if (movimento.matches("[A-H][0-7]")
                                && tablero.esValido(new MovimientoOthello(movimento))) {
                            evento.getPartida().realizaAccion(new AccionMover(
                                    this, new MovimientoOthello(movimento)));
                            break;
                        } else {
                            System.out.println("Wrong input, please try it again.");
                            count++;
                            if (count >= MAX_TRIES) {
                                System.out.println("To many wrong input. You pass this round.");
                                evento.getPartida().realizaAccion(new AccionMover(
                                        this, null));
                            }
                        }
                    } catch (ExcepcionJuego excepcionJuego) {
                        excepcionJuego.printStackTrace();
                    }
                }
                break;
            case Evento.EVENTO_FIN:
                if (observador) {
                    System.out.println(evento.getDescripcion());
                }
                break;
            case Evento.EVENTO_ERROR:
                System.out.println(evento.getDescripcion());
                break;
            default:
                System.out.println("Error: Undefined event type");
        }
    }
}
