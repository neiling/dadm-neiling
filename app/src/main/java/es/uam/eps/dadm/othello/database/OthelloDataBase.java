package es.uam.eps.dadm.othello.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import es.uam.eps.dadm.othello.activities.OtPreferenceActivity;
import es.uam.eps.dadm.othello.database.RoundDataBaseSchema.UserTable;
import es.uam.eps.dadm.othello.database.RoundDataBaseSchema.RoundTable;
import es.uam.eps.dadm.othello.model.Round;
import es.uam.eps.dadm.othello.model.RoundRepository;

/**
 * Interface to the database. Contains all dates of the users and rounds.
 */
public class OthelloDataBase implements RoundRepository {

    /**
     * Debug string.
     */
    private static final String DEBUG_TAG = "OthelloDataBase";

    /**
     * Name of the database.
     */
    private static final String DATABASE_NAME = "othello.db";

    /**
     * Version number of the database.
     */
    private static final int DATABASE_VERSION = 1;

    /**
     * Database helper.
     */
    private DatabaseHelper helper;

    /**
     * SQLite database.
     */
    private SQLiteDatabase db;

    /**
     * Constructor for a new instance of a OthelloDataBase.
     * @param context of the calling.
     */
    public OthelloDataBase(Context context) {
        this.helper = new DatabaseHelper(context);
    }

    /**
     * Helper class to crate the database.
     */
    private class DatabaseHelper extends SQLiteOpenHelper {

        /**
         * Constructor for a new instance of a DatabaseHelper.
         * @param context of the calling.
         */
        protected DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            createTable(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
            db.execSQL("DROP TABLE IF EXISTS " + UserTable.NAME);
            db.execSQL("DROP TABLE IF EXISTS " + RoundTable.NAME);
            createTable(db);
        }

        /**
         * @param db Database to be initialized.
         */
        private void createTable(SQLiteDatabase db) {
            String str1 = "CREATE TABLE " + UserTable.NAME + " ("
                    + "_id integer primary key autoincrement, "
                    + UserTable.Cols.PLAYERUUID + " TEXT UNIQUE, "
                    + UserTable.Cols.PLAYERNAME + " TEXT UNIQUE, "
                    + UserTable.Cols.PLAYERPASSWORD + " TEXT);";

            String str2 = "CREATE TABLE " + RoundTable.NAME + " ("
                    + "_id integer primary key autoincrement, "
                    + RoundTable.Cols.ROUNDUUID + " TEXT UNIQUE, "
                    + RoundTable.Cols.PLAYERUUID + " TEXT REFERENCES "
                    + UserTable.Cols.PLAYERUUID + ", "
                    + RoundTable.Cols.DATE + " TEXT, "
                    + RoundTable.Cols.TITLE + " TEXT, "
                    + RoundTable.Cols.SIZE + " TEXT, "
                    + RoundTable.Cols.BOARD + " TEXT);";
            try {
                db.execSQL(str1);
                db.execSQL(str2);
            } catch (SQLiteException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * @param round for the extracting the dates.
     * @return a ContenValues with all dates of the round.
     */
    private ContentValues getContentValues(Round round) {
        ContentValues values = new ContentValues();
        values.put(RoundTable.Cols.ROUNDUUID, round.getId());
        values.put(RoundTable.Cols.PLAYERUUID, round.getPlayerUUID());
        values.put(RoundTable.Cols.DATE, round.getDate());
        values.put(RoundTable.Cols.TITLE, round.getTitle());
        values.put(RoundTable.Cols.SIZE, round.getSize());
        values.put(RoundTable.Cols.BOARD, round.getBoard().tableroToString());
        return values;
    }

    /**
     * Query request for the rounds.
     * @return a RoundCursorWrapper.
     */
    private RoundCursorWrapper queryRounds() {
        String sql = "SELECT " + UserTable.Cols.PLAYERNAME + ", " +
                UserTable.Cols.PLAYERUUID + ", " +
                RoundTable.Cols.ROUNDUUID + ", " +
                RoundTable.Cols.DATE + ", " +
                RoundTable.Cols.TITLE + ", " +
                RoundTable.Cols.SIZE + ", " +
                RoundTable.Cols.BOARD + " " +
                "FROM " + UserTable.NAME + " AS p, " +
                RoundTable.NAME + " AS r " +
                "WHERE " + "p." + UserTable.Cols.PLAYERUUID + "=" +
                "r." + RoundTable.Cols.PLAYERUUID + ";";
        Cursor cursor = this.db.rawQuery(sql, null);
        return new RoundCursorWrapper(cursor);
    }

    @Override
    public void open() throws SQLiteException {
        this.db = this.helper.getWritableDatabase();
    }

    @Override
    public void close() {
        this.db.close();
    }

    @Override
    public void login(String playername, String playerpassword, LoginRegisterCallback callback) {
        Log.d(DEBUG_TAG, "Login " + playername);
        Cursor cursor = db.query(UserTable.NAME,
                new String[]{UserTable.Cols.PLAYERUUID},
                UserTable.Cols.PLAYERNAME + " = ? AND " + UserTable.Cols.PLAYERPASSWORD + " = ?",
                new String[]{playername, playerpassword}, null, null, null);
        int count = cursor.getCount();
        String uuid = count == 1 && cursor.moveToFirst() ? cursor.getString(0) : "";
        cursor.close();
        if (count == 1) {
            callback.onLogin(uuid);
        } else {
            callback.onError("Username or password incorrect.");
        }
    }

    @Override
    public void register(String playername, String password, LoginRegisterCallback callback) {
        ContentValues values = new ContentValues();
        String uuid = UUID.randomUUID().toString();
        values.put(UserTable.Cols.PLAYERUUID, uuid);
        values.put(UserTable.Cols.PLAYERNAME, playername);
        values.put(UserTable.Cols.PLAYERPASSWORD, password);
        long id = db.insert(UserTable.NAME, null, values);
        if (id < 0) {
            callback.onError("Error inserting new player named " + playername);
        } else {
            callback.onLogin(uuid);
        }
    }

    @Override
    public void getRounds(String playeruuid, String orderByField,
                          String group, RoundsCallback callback) {
        List<Round> rounds = new ArrayList<>();
        RoundCursorWrapper cursor = queryRounds();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Round round = cursor.getRound();
            if (round.getPlayerUUID().equals(playeruuid)) {
                rounds.add(round);
            }
            cursor.moveToNext();
        }
        cursor.close();
        if (cursor.getCount() > 0) {
            callback.onResponse(rounds);
        } else {
            callback.onError("No rounds found in database");
        }
    }

    @Override
    public void addRound(Round round, BooleanCallback callback) {
        if (OtPreferenceActivity.getOnline()) {
            round.setNumbersOfPlayer(2);
        }
        ContentValues values = getContentValues(round);
        long id = this.db.insert(RoundTable.NAME, null, values);
        if (callback != null) {
            callback.onResponse(id >= 0);
        }
    }


    @Override
    public void updateRound(Round round, BooleanCallback callback) {
        ContentValues values = getContentValues(round);
        long id = this.db.update(RoundTable.NAME, values,
                RoundTable.Cols.ROUNDUUID + " = ?", new String[]{round.getId()});
        if (callback != null) {
            callback.onResponse(id >= 0);
        }
    }
}
