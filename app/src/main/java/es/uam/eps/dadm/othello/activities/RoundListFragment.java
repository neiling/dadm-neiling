package es.uam.eps.dadm.othello.activities;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import es.uam.eps.dadm.othello.R;
import es.uam.eps.dadm.othello.model.OtLocalServerPlayer;
import es.uam.eps.dadm.othello.model.Round;
import es.uam.eps.dadm.othello.model.RoundRepository;
import es.uam.eps.dadm.othello.model.RoundRepositoryFactory;
import es.uam.eps.dadm.othello.server.ServerRepository;

/**
 * The fragment for a list of rounds.
 */
public class RoundListFragment extends Fragment {
    /**
     * Debug string.
     */
    private static final String DEBUG = "RoundListFragment";

    /**
     * Recycler for the round view.
     */
    private RecyclerView roundRecyclerView;

    /**
     * Adapter for the round.
     */
    private RoundAdapter roundAdapter;

    /**
     * Contains an callback object of the Callbacks interface.
     */
    private Callbacks callbacks;

    /**
     * Empty constructor for a new instance of a RoundListFragment.
     */
    public RoundListFragment() {
    }

    /**
     * Callback interface for round selected.
     */
    public interface Callbacks {

        /**
         * @param round which is selected.
         */
        void onRoundSelected(Round round);

        /**
         * If a preferences are selected.
         */
        void onPreferencesSelected();

        /**
         * Call for the help button.
         */
        void onHelpSelected();
    }

    /**
     * Notify the view over an update.
     */
    public void updateUI() {
        RoundRepository repository = RoundRepositoryFactory.createRepository(getActivity());
        RoundRepository.RoundsCallback roundsCallback = new RoundRepository.RoundsCallback() {
            @Override
            public void onResponse(List<Round> rounds) {
                roundAdapter = new RoundAdapter(rounds);
                roundRecyclerView.setAdapter(roundAdapter);
            }

            @Override
            public void onError(String error) {
                if (getView() != null) {
                    Snackbar.make(getView(), error,
                            Snackbar.LENGTH_LONG).show();
                }
            }
        };
        String playeruuid = OtPreferenceActivity.getPlayerUUID(getActivity());
        repository.getRounds(playeruuid, null, null, roundsCallback);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.callbacks = (Callbacks) activity;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.callbacks = (Callbacks) context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_round_list, container, false);

        this.roundRecyclerView = (RecyclerView) view.findViewById(R.id.round_recycler_view);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        this.roundRecyclerView.setLayoutManager(linearLayoutManager);
        this.roundRecyclerView.setItemAnimator(new DefaultItemAnimator());

        updateUI();
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_round:
                Round round = new Round(Integer
                        .parseInt(OtPreferenceActivity.getBoardSize(getActivity())));

                String playeruuid = OtPreferenceActivity.getPlayerUUID(getActivity());
                round.setPlayerUUID(playeruuid);
                round.setFirstPlayerName(OtPreferenceActivity.getPlayerName());

                RoundRepository repository = RoundRepositoryFactory.createRepository(getActivity());
                RoundRepository.BooleanCallback callback = new RoundRepository.BooleanCallback() {
                    @Override
                    public void onResponse(boolean ok) {
                        Log.d(DEBUG, "New round added.");
                        updateUI();
                    }
                    @Override
                    public void onError(String error) {
                        Log.d(DEBUG, "Error by adding round.");
                    }
                };
                repository.addRound(round, callback);
                return true;
            case R.id.menu_item_settings:
                this.callbacks.onPreferencesSelected();
                return true;
            case R.id.menu_item_help:
                this.callbacks.onHelpSelected();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.callbacks = null;
    }

    /**
     * Hold the view of the round.
     */
    private class RoundHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        /**
         * Text view of the round id.
         */
        private TextView idTextView;

        /**
         * Text view withe the current game state.
         */
        private TextView gameStateTextView;

        /**
         * Date when the round started.
         */
        private TextView dateTextView;

        /**
         * Name of the players.
         */
        private TextView playerTextView;

        /**
         * Round.
         */
        private Round round;

        /**
         * Generate a new RoundHolder and pulled the views from R.
         *
         * @param itemView the current view.
         */
        private RoundHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.idTextView = (TextView) itemView.findViewById(R.id.list_item_id);
            this.gameStateTextView = (TextView) itemView.findViewById(R.id.list_item_board);
            this.dateTextView = (TextView) itemView.findViewById(R.id.list_item_date);
            this.playerTextView = (TextView) itemView.findViewById(R.id.list_item_players);
        }

        /**
         * @param round fill the views with dates.
         */
        private void bindRound(Round round) {
            this.round = round;
            this.idTextView.setText(round.getTitle());
            this.gameStateTextView.setText(round.getBoard().toSimpleString());
            this.dateTextView.setText(String.valueOf(round.getDate().substring(0, 19)));
            this.playerTextView.setText(round.getFirstPlayerName() +
                    " vs. " + round.getSecondPlayerName());
        }

        @Override
        public void onClick(View v) {
            if (this.round.getNumbersOfPlayer() == 1 &&
                    !this.round.getFirstPlayerName().
                            equals(OtPreferenceActivity.getPlayerName()) ) {
                ServerRepository repository = (ServerRepository)
                        RoundRepositoryFactory.createRepository(getActivity());
                assert repository != null;
                repository.addPlayerToRound(this.round, null);
                callbacks.onRoundSelected(this.round);
            } else {
                callbacks.onRoundSelected(this.round);
            }

        }

    }

    /**
     * The adapter for the rounds.
     */
    private class RoundAdapter extends RecyclerView.Adapter<RoundHolder> {

        /**
         * All started rounds.
         */
        private List<Round> rounds;

        /**
         * @param rounds all rounds.
         */
        private RoundAdapter(List<Round> rounds) {
            this.rounds = rounds;
        }

        @Override
        public RoundHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.list_item_round, parent, false);
            return new RoundHolder(view);
        }

        @Override
        public void onBindViewHolder(RoundHolder holder, int position) {
            Round round = this.rounds.get(position);
            holder.bindRound(round);
        }

        @Override
        public int getItemCount() {
            return this.rounds.size();
        }

    }
}
