package es.uam.eps.dadm.othello.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import es.uam.eps.dadm.othello.R;
import es.uam.eps.dadm.othello.model.Round;

/**
 * Activity witch start a with list of the games/rounds.
 */
public class RoundListActivity extends AppCompatActivity implements
        RoundListFragment.Callbacks, RoundFragment.Callbacks {

    /**
     * Empty constructor for a new instance of a RoundListActivity.
     */
    public RoundListActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_masterdetail);

        FragmentManager fm = getFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);

        if (fragment == null) {
            fragment = new RoundListFragment();
            fm.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }
    }

    @Override
    public void onRoundSelected(Round round) {
        if (findViewById(R.id.detail_fragment_container) == null) {
            Intent intent = RoundActivity.newIntent(this, round.getId(), round.getFirstPlayerName(),
                    round.getTitle(), round.getSize(), round.getDate(),
                    round.getBoard().tableroToString());
            startActivity(intent);
        } else {
            RoundFragment roundFragment = RoundFragment.newInstance(round.getId(),
                    round.getFirstPlayerName(), round.getTitle(),
                    "" + round.getSize(), round.getDate(), round.getBoard().tableroToString());
            getFragmentManager().beginTransaction()
                    .replace(R.id.detail_fragment_container, roundFragment)
                    .commit();
        }
    }

    @Override
    public void onPreferencesSelected() {
        Intent intent = new Intent(this, OtPreferenceActivity.class);
        startActivity(intent);
    }

    @Override
    public void onHelpSelected() {
        Intent intent = new Intent(this, HelpActivity.class);
        startActivity(intent);
    }

    @Override
    public void onRoundUpdated(Round round) {
        FragmentManager fm = getFragmentManager();
        RoundListFragment roundListFragment =
                (RoundListFragment) fm.findFragmentById(R.id.fragment_container);
        roundListFragment.updateUI();
    }


}
