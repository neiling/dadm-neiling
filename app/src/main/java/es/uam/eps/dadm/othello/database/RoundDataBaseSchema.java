package es.uam.eps.dadm.othello.database;

/**
 * The schema for the round database.
 */
public class RoundDataBaseSchema {

    /**
     * Empty constructor for a new instance of a RoundDataBaseSchema.
     */
    public RoundDataBaseSchema() {
    }

    /**
     * Table of the users.
     */
    public static final class UserTable {

        /**
         * Name of the users table.
         */
        public static final String NAME = "users";

        /**
         * Empty constructor for a new instance of a UserTable.
         */
        public UserTable() {
        }

        /**
         * Cols of the user table.
         */
        public static final class Cols {

            /**
             * Id of the player.
             */
            public static final String PLAYERUUID = "playeruuid1";

            /**
             * Name of the player.
             */
            public static final String PLAYERNAME = "playername";

            /**
             * Password of the player.
             */
            public static final String PLAYERPASSWORD = "playerpassword";

            /**
             * Empty constructor for a new instance of a Cols.
             */
            public Cols() {
            }
        }

    }

    /**
     * Table of the rounds.
     */
    public static final class RoundTable {

        /**
         * Name of the table.
         */
        public static final String NAME = "rounds";

        /**
         * Empty constructor for a new instance of a RoundTable.
         */
        public RoundTable() {
        }

        /**
         * Cols of the round table.
         */
        public static final class Cols {

            /**
             * Id of the player.
             */
            public static final String PLAYERUUID = "playeruuid2";

            /**
             * Id of the round.
             */
            public static final String ROUNDUUID = "rounduuid";

            /**
             * Date of the round.
             */
            public static final String DATE = "date";

            /**
             * Title of the round.
             */
            public static final String TITLE = "title";

            /**
             * Size of the board.
             */
            public static final String SIZE = "size";

            /**
             * All dates of the board as json string.
             */
            public static final String BOARD = "board";

            /**
             * Empty constructor for a new instance of a Cols.
             */
            public Cols() {
            }
        }
    }

}
