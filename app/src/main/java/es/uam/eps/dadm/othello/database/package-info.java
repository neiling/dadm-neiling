/**
 * Contains the database structure.
 *
 * @author neiling
 * @version 1.0
 * @since 1.0
 */
package es.uam.eps.dadm.othello.database;
