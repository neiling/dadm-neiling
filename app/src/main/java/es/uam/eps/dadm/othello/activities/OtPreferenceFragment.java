package es.uam.eps.dadm.othello.activities;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import es.uam.eps.dadm.othello.R;

/**
 * Call the preference xml for the fragments.
 */
public class OtPreferenceFragment extends PreferenceFragment {

    /**
     * Empty constructor for a new instance of a OtPreferenceFragment.
     */
    public OtPreferenceFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
    }
}
