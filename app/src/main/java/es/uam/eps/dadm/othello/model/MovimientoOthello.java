package es.uam.eps.dadm.othello.model;

import es.uam.eps.multij.Movimiento;

/**
 * @author neiling
 */
public class MovimientoOthello extends Movimiento {

    /**
     * Contiene una cadena con las coordenadas del proximo movimiento.
     * De la forma "B3" para "columna-fila"
     */
    private String coordenadas;

    /**
     * Constructor para una nueva instancia de MovimientoOthello.
     *
     * @param coordenadas una cadena de la forma "C2"
     */
    public MovimientoOthello(String coordenadas) {
        this.coordenadas = coordenadas;
    }

    @Override
    public String toString() {
        return this.coordenadas;
    }

    @Override
    public boolean equals(Object o) {
        return (o.getClass() == getClass() && o.toString().equals(this.coordenadas)) || o == this;
    }

}
