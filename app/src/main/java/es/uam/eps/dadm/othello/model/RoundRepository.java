package es.uam.eps.dadm.othello.model;

import android.database.sqlite.SQLiteException;

import java.util.List;

/**
 * Interface for a round repository.
 */
public interface RoundRepository {

    /**
     * Open the repository.
     * @throws SQLiteException if an open of the repo. is not possible.
     */
    void open() throws SQLiteException;

    /**
     * Close the repository.
     */
    void close();

    /**
     * Callback interface for a registration or login.
     */
    interface LoginRegisterCallback {

        /**
         * @param playerUuid of the player.
         */
        void onLogin(String playerUuid);

        /**
         * @param error if somethings works wrong.
         */
        void onError(String error);
    }

    /**
     * @param playername name of the player.
     * @param password form the player.
     * @param callback for login.
     */
    void login(String playername, String password, LoginRegisterCallback callback);

    /**
     * @param playername name of the player.
     * @param password form the player.
     * @param callback for registration.
     */
    void register(String playername, String password, LoginRegisterCallback callback);

    /**
     * Boolean callback interface.
     */
    interface BooleanCallback {

        /**
         * @param ok true if response was successful.
         */
        void onResponse(boolean ok);

        /**
         * @param error if somethings works wrong.
         */
        void onError(String error);
    }

    /**
     * @param playeruuid form the player.
     * @param orderByField Specifies the database request.
     * @param group Specifies the database request.
     * @param callback for the rounds.
     */
    void getRounds(String playeruuid, String orderByField, String group, RoundsCallback callback);

    /**
     * @param round which should be add.
     * @param callback true if adding was successful.
     */
    void addRound(Round round, BooleanCallback callback);

    /**
     * @param round which should be update.
     * @param callback true if updating was successful.
     */
    void updateRound(Round round, BooleanCallback callback);

    /**
     * Callback of the rounds.
     */
    interface RoundsCallback{

        /**
         * @param rounds a list with all rounds.
         */
        void onResponse(List<Round> rounds);

        /**
         * @param error message if something works wrong by callback.
         */
        void onError(String error);
    }

}