/**
 * Contains the activities of the app.
 *
 * @author neiling
 * @version 1.0
 * @since 1.0
 */
package es.uam.eps.dadm.othello.activities;
